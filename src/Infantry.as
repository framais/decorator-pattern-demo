package  
{
	public interface Infantry 
	{
		function get damage():int;
		function get armor():int;
	}	
}