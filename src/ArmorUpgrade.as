package  
{

	public class ArmorUpgrade extends InfantryDecorator 
	{
		
		public function ArmorUpgrade(infantry:Infantry) 
		{
			super(infantry);
		}
		
		override public function get armor():int
		{
			return super.armor + 1;
		}		
	}
}