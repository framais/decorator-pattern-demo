package  
{
	public class Marauder implements Infantry 
	{
		
		/* INTERFACE Infantry */
		
		public function get damage():int 
		{
			return 10;
		}
		
		public function get armor():int 
		{
			return 1;
		}
		
	}

}