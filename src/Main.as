package 
{
	import flash.display.Sprite;	

	public class Main extends Sprite 
	{		
		public function Main():void 
		{
			var marine:Infantry = new Marine();
			var marauder:Infantry = new Marauder();
			
			/* MARINE WEAPON UPGRADE */
			trace("Marine damage before weapon upgrade = " + marine.damage);			
			marine = new WeaponUpgrade(marine);
			trace("Marine damage after weapon upgrade = " + marine.damage);
			
			/* MARINE ARMOR UPGRADES */
			trace("3:Marine armor before armor upgrade = " + marine.armor);
			marine = new ArmorUpgrade(marine);
			trace("3:Marine armor after 1st armor upgrade = " + marine.armor);			
			marine = new ArmorUpgrade(marine);
			trace("3:Marine armor after 2nd armor upgrade = " + marine.armor);			
			marine = new ArmorUpgrade(marine);
			trace("3:Marine armor after 3rd armor upgrade = " + marine.armor);
			
			/* MARAUDER WEAPON UPGRADE */
			trace("4:Marauder damage before weapon upgrade = " + marauder.damage);
			marauder = new WeaponUpgrade(marauder);
			trace("4:Marauder damage after weapon upgrade = " + marauder.damage);
		}		
	}	
}