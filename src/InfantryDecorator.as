package  
{

	public class InfantryDecorator implements Infantry 
	{
		protected var infantry:Infantry;
		
		public function InfantryDecorator(infantry:Infantry) 
		{
			this.infantry = infantry;
		}
		
		/* INTERFACE Infantry */
		
		public function get damage():int 
		{
			return infantry.damage;
		}
		
		public function get armor():int 
		{
			return infantry.armor;
		}		
	}
}