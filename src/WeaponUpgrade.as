package  
{
	/**
	 * ...
	 * @author Francesco Maisto
	 */
	public class WeaponUpgrade extends InfantryDecorator 
	{
		
		public function WeaponUpgrade(infantry:Infantry) 
		{
			super(infantry);
		}
		
		override public function get damage():int
		{
			return super.damage + 1;
		}		
	}
}